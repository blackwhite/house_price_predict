#FROM ubuntu-python-base:0.1
FROM graphcreate:1.7.1

RUN apt-get -y install \
    libpq5 \
	libpq-dev

RUN mkdir app
RUN mkdir app/requirements

WORKDIR app

EXPOSE 8000

ADD requirements.txt requirements

RUN pip install -r requirements/requirements.txt

CMD gunicorn -b 0.0.0.0:8000 api:app
