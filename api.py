# -*- coding: utf-8 -*-

import json
import logging
import falcon
from services.predict import PredictorService


ALLOWED_ORIGINS = ['http://localhost:9000']


class CorsMiddleware(object):

    def process_request(self, request, response):
        origin = request.get_header('Origin')
        if origin in ALLOWED_ORIGINS:
            logging.info(origin)
            response.set_header('Access-Control-Allow-Origin', 'http://localhost:9000')
            response.set_header('Access-Control-Allow-Headers', 'Content-Type')
            response.set_header('Access-Control-Allow-Methods', 'POST')
        logging.info(origin)


class PredictorResource(object):

    def __init__(self, model_path):
        self._predictor_service = PredictorService(model_path)

    def on_post(self, req, resp):
        data = json.loads(req.stream.read())
        predicted_price = self._predictor_service.predict(data)

        resp.body = str(predicted_price)


app = application = falcon.API(middleware=[CorsMiddleware()])

app.add_route('/predictor', PredictorResource('house_price_model'))
