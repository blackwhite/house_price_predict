from sqlalchemy.ext.declarative import DeclarativeMeta

import json
from decimal import Decimal
from datetime import date


class AlchemyJSONEncoder(json.JSONEncoder):
    '''
    https://gist.github.com/KodeKracker/b44dea8df6c0c90fdcdb
    '''
    def default(self, o):
        # check if object `o` is of custom declared model instance
        if isinstance(o.__class__, DeclarativeMeta):
            data = {}
            fields = o.__json__() if hasattr(o, '__json__') else dir(o)
            for field in [f for f in fields if not f.startswith('_') and
                          f not in ['metadata', 'query', 'query_class']]:
                value = o.__getattribute__(field)
                try:
                    if json.dumps(value):
                        data[field] = value
                except TypeError:
                    data[field] = None
            return data
        # check if object `o` is of Decimal instance
        elif isinstance(o, Decimal):
            return o.to_eng_string()
        # check if object `o` is of date instance
        elif isinstance(o, date):
            return o.isoformat()
        # rest of objects are handled by default JSONEncoder like 'Datetime',
        # 'UUID', 'Markdown' and various others
        return json.JSONEncoder.default(self, o)


def model_as_json(obj):
    return json.dumps(obj, cls=AlchemyJSONEncoder)


def dict_to_model(data, cls):
	inst = cls()

	for key in data:
		inst.__setattr__(key, data[key])

	return inst
