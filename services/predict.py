import graphlab


class PredictorService(object):

    def __init__(self, model_path):
        self._model = graphlab.load_model(model_path)


    def predict(self, data):
        result = self._model.predict(graphlab.SFrame(data))

        return result[0]
